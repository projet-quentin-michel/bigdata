**BIG DATA SIRENE INVADER PROJECT**

SIRENE Invader V2 transforms a large CSV of French companies into an efficient MongoDB database. We break down the file for easier handling and use PM2 for fast, parallel processing. The project is documented and tracked on Git.

**HOW TO CLONE THE PROJECT**

- Clone the Git Repository
- Open your terminal.
- Go to the folder where you want to clone the project using cd path/to/folder.
- Run git clone project_git_url
- Run npm install
- Create a .env file in the project's root directory.
- Add your databa Url like this DATABASE_URL=mongodb://your_mongodb_url

**ALL PACKAGES USED**

**Mongoose:** 

A package that makes it easier to work with MongoDB databases by letting you structure your data with models and schemas.

**fs-extra:** 

An extension of the basic file handling package in Node.js, adding more capabilities like copying and moving files more easily.

**csv-split-stream:** 

Helps break down large CSV files into smaller, more manageable files. It's great for processing big datasets piece by piece.

**csv-parse:** 

A tool for turning CSV text input into readable data arrays or objects. It's usefull for extracting data from CSV files to use in your applications.

**dotenv:** 

Keeps secret settings in a separate file, making your app safer.

**DATABASE USED** 

**MongoDB**

In this project, MongoDB is the main database. It keeps a lot of company data and lets us find and use that data quickly. It works well with the big CSV files we have, making it easy to get the information we need.







